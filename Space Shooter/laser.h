#ifndef LASER_H
#define LASER_H

#include "objeto_jogo.h"
/**
*   Classe Laser.
*   Esta classe herda a classe ObjetoJogo e tem como fução criar os lasers.
*
*   /author Felipe Marinho de Freitas.
*   /since 04/12/2020
*/

class Laser: public ObjetoJogo
{
    public:
        /**Construtor
            * /param folhaTextura, um ponteiro constante que aponta para um array de char que é o diretório da imagem
            * /param px, um inteiro que representa a posição de x do objeto.
            * /param py, um inteiro que representa a posição de y do objeto.
            * /param vx, um inteiro que representa a velocidade no eixo x.
            * /param vy, um inteiro que representa a velocidade no eixo y.
        */
        Laser(SDL_Renderer *renderer, const char *folhaTextura, int px, int py, int vx, int vy);
        ///Destrutor
        ~Laser();
};

#endif // LASER_H
