#ifndef JOGO_H
#define JOGO_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
using namespace std;

#include "fundo.h"
#include "jogador.h"
#include "laser.h"
#include "meteorito.h"


/**
*   Classe Jogo.
*   Esta classe principal recebe todas as outras para formar o corpo do jogo.
*
*   /author Felipe Marinho de Freitas.
*   /since 04/12/2020
*/

class Jogo
{
    public:
        ///Construtor vazio.
        Jogo();
        ///Destrutor vazio.
        ~Jogo();
        /**
            *Função que inicializa a biblioteca SDL,cria a janela e o renderizar, e onde é criado os objetos.
            * /param titulo, um ponteiro constante para um char que vai ser o titulo da janela.
            * /param comprimento, um inteiro que é o comprimento da janela.
            * /param altura, um inteiro que é a altura da janela.
        */
        void inicializar (const char *titulo, int comprimento, int altura);
        /**
            *Função que recebe todos os eventos do jogo.
        */
        void gerenciarEventos ();
        /**
            *Função que atualiza todos os objetos na tela.
        */
        void atualizar();
        /**
            *Função que limpa e renderiza todos os objetos na tela.
        */
        void renderizar();
        /**
            *Função que destroi a janela,renderizador e sai deles.
        */
        void limpar();
        /**
            *Função que retorna uma variavel que indica se o jogo ainda esta rodando.
            * /return correndo
        */
        bool getCorrendo();

    private:
        ///< Variavel correndo do tipo bool para confirmar se o jogo esta correndo.
        bool correndo;
        ///< Ponteiro janela para SDL_Window.
        SDL_Window *janela;

        ///< Ponteiro para renderizador
        SDL_Renderer *renderizador;
        int sorteio;

        Fundo fundo;
        Jogador jog1;



        //// variável para manter o controle dos objetos laser
        vector<Laser *> lasers;
        vector <Meteorito *> meteoritos;

};

#endif // JOGO_H
