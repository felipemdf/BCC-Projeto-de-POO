#include "classes/Jogo.h"

Jogo *jogo = nullptr;

int main (int argc, const char *argv[])
{
    const int FPS=60;
    const int frameDelay= 1000/FPS; //1 segundo dividido por 60 quadros
    Uint32 frameInicio;
    int frameTempo;

    jogo=new Jogo();
    jogo->inicializar("Space Shooter",600,700);
    while (jogo->getCorrendo())
    {
        frameInicio=SDL_GetTicks();

        jogo->gerenciarEventos();
        jogo->atualizar();
        jogo->renderizar();

        frameTempo=SDL_GetTicks()-frameInicio;
        if (frameDelay>frameTempo) //se o tempo de realizar as aços for menor que o delay, criar um delay para complementar.
        {
            SDL_Delay(frameDelay-frameTempo);
        }
    }
    jogo->limpar();
}
