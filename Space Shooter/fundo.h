#ifndef FUNDO_H
#define FUNDO_H
#include <SDL2/SDL.h>

class Fundo
{
    public:
        Fundo();
        Fundo(SDL_Renderer *renderizador, const char *folhaTextura);
        ~Fundo();

        void atualizar();
        void renderizar(SDL_Renderer *renderizador);

    protected:
        float contador;
        SDL_Texture *texturaFundo;
        SDL_Rect origemRet, destinoRet, destinoRet2;

};

#endif // FUNDO_H
