#include "meteorito.h"
#include "gerenciador_textura.h"
#include <stdlib.h>
Meteorito::Meteorito(){}

Meteorito::Meteorito(SDL_Renderer *renderer, const char *folhaTextura, int vx, int vy)
{
    texturaObjeto = GerenciadorTextura::carregarTextura(renderer, folhaTextura);
    comprimento = 80;
    altura = 80;
    posX = rand()%(550+1);
    posY = 0-altura;
    velX = vx;
    velY = vy;
}

Meteorito::~Meteorito()
{
}
