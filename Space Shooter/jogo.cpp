#include "jogo.h"
/*
#include "GerenciadorTextura.h"
#include "Fundo.h"
#include "Jogador.h"
#include "Laser.h"
#include <list>
*/

/*
Fundo *fundo;
Jogador *jog1;
Laser *laser_jog1;
*/
Jogo::Jogo()
{}

Jogo::~Jogo()
{}

void Jogo::inicializar (const char *titulo, int comprimento, int altura)
{
    if (SDL_Init(SDL_INIT_EVERYTHING)==0)
    {
        cout << "SDL iniciado..." << endl;
        janela=SDL_CreateWindow(titulo,SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,comprimento,altura,0);

        if (janela)
        {
            cout << "Janela criado..." << endl;
        }
        renderizador=SDL_CreateRenderer(janela,-1,0);

        if (renderizador)
        {
            SDL_SetRenderDrawColor(renderizador,255,255,255,255);
            cout << "Renderizador criado..." << endl;
        }
        correndo=true;
    }
    fundo = Fundo(renderizador, "imagens/fundo2final.jpg");
    jog1 = Jogador(renderizador, "imagens/naves/nave1.png");

}

void Jogo::gerenciarEventos ()
{
    SDL_Event e;
    while ( SDL_PollEvent(&e)!=0)
    {
        if (e.type==SDL_QUIT)
        {
            correndo=false;
        }

        if (e.type==SDL_KEYDOWN && e.key.repeat==0)
        {
            switch (e.key.keysym.sym)
            {
                case SDLK_UP: {
                    jog1.addVelY(-5);
                    break;
                }
                case SDLK_DOWN : {
                    jog1.addVelY(+5);
                    break;
                }
                case SDLK_LEFT: {
                    jog1.addVelX(-5);
                    break;
                }
                case SDLK_RIGHT: {
                    jog1.addVelX(+5);
                    break;
                }
                case SDLK_SPACE: {
                    cout << "laser" << endl;
                    Laser * l = new Laser(renderizador, "imagens/efeitos/laser.png", jog1.getPosX()+30, jog1.getPosY()-50, 0, -10);
                    lasers.push_back(l);
                    break;
                }
                case SDLK_l: {
                    cout << "lasers.size: " << lasers.size() << endl;
                    for (int i = 0; i < lasers.size(); i++) {
                        cout << "(" << lasers[i]->getPosX() << "," << lasers[i]->getPosY() << ")" << endl;
                    }
                    break;
                }
                default:
                break;
            }
        }

         if (e.type==SDL_KEYUP && e.key.repeat==0)
        {
            switch (e.key.keysym.sym)
            {
                case SDLK_UP: {
                    jog1.addVelY(+5);
                    break;
                }
                case SDLK_DOWN : {
                    jog1.addVelY(-5);
                    break;
                }
                case SDLK_LEFT: {
                    jog1.addVelX(+5);
                    break;
                }
                case SDLK_RIGHT: {
                    jog1.addVelX(-5);
                    break;
                }
                default:
                    break;
            }
        }
    }
    jog1.mover();
    for (int i = 0; i < lasers.size(); i++)
    {
        lasers[i]->mover();
    }
    //METEOROS
    sorteio = rand() % 1000 + 1;
    if (sorteio == 1)
    {
        Meteorito *m = new Meteorito(renderizador,"imagens/meteorito.png",0,+3);
        meteoritos.push_back(m);
    }
    for (int i = 0; i < meteoritos.size(); i++)
    {
        meteoritos[i]->mover();
    }
}

void Jogo::atualizar()
{
    fundo.atualizar();
    jog1.atualizar();
    for (int i = 0; i < lasers.size(); i++)
        lasers[i]->atualizar();
    for (int i = 0; i < meteoritos.size(); i++)
        meteoritos[i]->atualizar();
}

void Jogo::renderizar()
{
    SDL_RenderClear(renderizador);
    fundo.renderizar(renderizador);
    jog1.renderizar(renderizador);
    for (int i = 0; i < lasers.size(); i++)
        lasers[i]->renderizar(renderizador);
    for (int i = 0; i < meteoritos.size(); i++)
        meteoritos[i]->renderizar(renderizador);
    SDL_RenderPresent(renderizador);
}

void Jogo::limpar()
{
    SDL_DestroyWindow(janela);
    SDL_DestroyRenderer(renderizador);
    SDL_Quit();
}

bool Jogo::getCorrendo()
{
    return correndo;
}
