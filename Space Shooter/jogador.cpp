#include "jogador.h"
#include "gerenciador_textura.h"
#include <iostream>
using namespace std;

Jogador::Jogador()
{
    comprimento=70;
    altura=70;
    posX = 250;
    posY =600;
    velX=0;
    velY=0;
}

Jogador::Jogador(SDL_Renderer *renderizador, const char *folhaTextura)
{
    texturaObjeto = GerenciadorTextura::carregarTextura(renderizador, folhaTextura);
    comprimento=70;
    altura=70;
    posX = 250;
    posY =600;
    velX=0;
    velY=0;
}

Jogador::~Jogador()
{}

void Jogador::eventojog(SDL_Event &e)
{
    /*
	if (e.type==SDL_KEYDOWN && e.key.repeat==0)
	{
        switch (e.key.keysym.sym)
        {
            case SDLK_UP:
                velY -= 10;
                break;
            case SDLK_DOWN:
                velY += 10;
                break;
            case SDLK_LEFT:
                velX -= 10;
                break;
            case SDLK_RIGHT:
                velX += 10;
                break;
        }
	}*/
}

void Jogador::mover()
{
    posX += velX;
    if ((posX < 0) || (posX + comprimento > 600))
    {
        posX -= velX;
    }

    posY += velY;
    if ((posY < 0) || (posY +altura > 700))
    {
        posY -= velY;
    }
}
