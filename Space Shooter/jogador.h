#ifndef JOGADOR_H
#define JOGADOR_H

#include "objeto_jogo.h"
class Jogador: public ObjetoJogo
{
    public:
        Jogador();
        Jogador(SDL_Renderer *renderizador, const char *folhaTextura);
        ~Jogador();

        void eventojog(SDL_Event &e);
        void mover ();
};


#endif // JOGADOR_H
