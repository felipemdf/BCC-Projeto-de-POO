#include "objeto_jogo.h"
#include "gerenciador_textura.h"


void ObjetoJogo::atualizar()
{
    //Posições
    origemRet.x = 0;
    origemRet.y = 0;
    origemRet.h = altura;
    origemRet.w = comprimento;

    destinoRet.x = posX;
    destinoRet.y = posY;
    destinoRet.h = altura;
    destinoRet.w = comprimento;
}

void ObjetoJogo::renderizar(SDL_Renderer *renderizador)
{
    SDL_RenderCopy(renderizador, texturaObjeto, NULL, &destinoRet);
}

void ObjetoJogo::mover()
{
    posX += velX;
    if ((posX < 0) || (posX + comprimento > 600))
    {
        posX -= velX;
    }

    posY += velY;
    if ((posY+altura < 0) || (posY +altura > 700))
    {
        posY -= velY;
    }
}
