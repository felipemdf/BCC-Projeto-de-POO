#include <SDL2/SDL.h>
#include "fundo.h"
#include "gerenciador_textura.h"


Fundo::Fundo() {
    texturaFundo = nullptr;
    contador = 0;
}

Fundo::Fundo(SDL_Renderer *renderizador, const char *folhaTextura)
{
    texturaFundo = GerenciadorTextura::carregarTextura(renderizador, folhaTextura);
    contador=0;
}

Fundo::~Fundo()
{}

void Fundo::atualizar()
{
    contador+=1;
    origemRet.x = 0;
    origemRet.y = 0;
    origemRet.h = 1400;
    origemRet.w = 600;


    destinoRet.x=0;
    destinoRet.y=contador;
    destinoRet.h = 1400;
    destinoRet.w = 600;

    destinoRet2.x=0;
    destinoRet2.y=contador-1400;
    destinoRet2.h = 1400;
    destinoRet2.w = 600;
    if (contador >= 1400)
    {
        contador=0;
    }
}

void Fundo::renderizar(SDL_Renderer *renderizador)
{
    SDL_RenderCopy(renderizador,texturaFundo,&origemRet,&destinoRet);
    SDL_RenderCopy(renderizador,texturaFundo,&origemRet,&destinoRet2);
}
