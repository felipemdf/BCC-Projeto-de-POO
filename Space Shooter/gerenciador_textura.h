#ifndef GERENCIADORTEXTURA_H
#define GERENCIADORTEXTURA_H

class GerenciadorTextura
{
    public:
        static SDL_Texture *carregarTextura (SDL_Renderer *renderizador, const char *caminho);
};

#endif // GERENCIADORTEXTURA_H
