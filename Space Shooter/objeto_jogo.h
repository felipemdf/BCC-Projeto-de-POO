#ifndef OBJETOJOGO_H
#define OBJETOJOGO_H

#include <SDL2/SDL.h>

/**
*   Classe ObjetoJogo.
*   Esta classe cria as principais variaveis de posição e movimentação, e funções para qualquer objeto.
*
*   /author Felipe Marinho de Freitas.
*   /since 04/12/2020
*/
class ObjetoJogo
{
    public:
        ///Função atualizar, que atualiza as posições.
        void atualizar ();
        ///Função renderizar, que renderiza o objeto.
        void renderizar(SDL_Renderer *renderizador);

        ///Função getPosX que retorna a posição de X.
        int getPosX (){return posX;}
        ///Função getPosY que retorna a posição de Y.
        int getPosY (){return posY;}
        ///Função getVelX que retorna a velocidade de X.
        int getVelX (){return velX;}
        ///Função getVelY que retorna a velocidade de Y.
        int getVelY (){return velY;}
        ///Função getComprimento que retorna o comprimento do objeto.
        int getComprimento (){return comprimento;}
        ///Função getAltura que retorna a altura do objeto.
        int getAltura (){return altura;}

        void addVelX(int q) { velX += q; }
        void addVelY(int q) { velY += q; }

        /** Função mover.
            *Esta função move o objeto com base na velocidade obtida impede o objeto de passar da área da tela
        */
        void mover();
    protected:
        ///< Variavel posX, do tipo inteiro que recebe a posição X do objeto.
        int posX;
        ///< Variavel posY, do tipo inteiro que recebe a posição Y do objeto.
        int posY;
        ///< Variavel velX, do tipo inteiro que recebe a velocidade de X do objeto.
        int velX;
        ///< Variavel velY, do tipo inteiro que recebe a velocidade de Y do objeto.
        int velY;
        ///< Variavel aceleracao, do tipo inteiro que recebe a aceleracao do objeto quando a tecla e pressionada.
        int aceleracao;
        ///< Variavel comprimento, do tipo inteiro que recebe o comprimento do objeto.
        int comprimento;
        ///< Variavel altura, do tipo inteiro que recebe a altura do objeto.
        int altura;

        ///< Ponteiro texturaObejto que aponta para uma textura do tipo SDL_Texture.
        SDL_Texture *texturaObjeto;
        ///< Variaveis origemRet e destinoRet, do tipo SDL_Rect que recebem as dimensões e posições dos objetos.
        SDL_Rect origemRet, destinoRet;
};

#endif // OBJETOJOGO_H
