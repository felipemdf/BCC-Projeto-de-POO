#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "gerenciador_textura.h"

SDL_Texture *GerenciadorTextura::carregarTextura(SDL_Renderer *renderizador, const char *caminho)
{
    SDL_Surface *superficieTemp = IMG_Load(caminho);
    SDL_Texture *tex = SDL_CreateTextureFromSurface(renderizador, superficieTemp);
    SDL_FreeSurface(superficieTemp);

    return tex;
}
