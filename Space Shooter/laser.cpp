#include "laser.h"
#include "gerenciador_textura.h"

Laser::Laser(SDL_Renderer *renderer, const char *folhaTextura, int px, int py, int vx, int vy)
{
    texturaObjeto = GerenciadorTextura::carregarTextura(renderer, folhaTextura);
    comprimento = 10;
    altura = 70;
    posX = px;
    posY = py;
    velX = vx;
    velY = vy;
}

