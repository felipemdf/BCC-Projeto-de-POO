#ifndef METEORITO_H
#define METEORITO_H

#include "objeto_jogo.h"
class Meteorito:public ObjetoJogo
{
    public:
        Meteorito ();
        Meteorito(SDL_Renderer *renderer,const char *folhaTextura, int vx, int vy);
        ~Meteorito();
    private:
        int vida;

};

#endif // METEORITO_H
