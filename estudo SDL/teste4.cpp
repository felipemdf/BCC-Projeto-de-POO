#include <iostream>
#include <SDL2/SDL.h>
#include <string>

using namespace std;

const int SCREEN_WIDTH= 640;
const int SCREEN_HEIGHT=480;

bool init ();

bool loadMedia ();

void close ();

SDL_Window *window =NULL;
SDL_Surface *currentSurface =NULL;
SDL_Surface *keyPresSurfaceUp=NULL;
SDL_Surface *screenSurface = NULL;
SDL_Surface *loadSurface (string path);

bool init(){
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO)<0){
        cout << "Erro ou iniciar" << endl;
        success=false;
    }
    else{
        window=SDL_CreateWindow("Teste SDL",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,SCREEN_WIDTH,SCREEN_HEIGHT,SDL_WINDOW_SHOWN);
        if (window == NULL){
            cout << "Erro ao iniciar a janela" << endl;
            success=false;
        }
        else{
            screenSurface=SDL_GetWindowSurface(window);
        }

    }
    return success;
}

bool loadMedia(){
    bool success = true;
    keyPresSurfaceUp = loadSurface("./imagens/jojofinal.bmp");
    if (keyPresSurfaceUp == NULL){
        cout << "Nao foi possivel encontrar a imagem" << endl;
        success=false;
    }
    return success;
}

SDL_Surface *loadSurface(string path){
        SDL_Surface *optimizedSurface =NULL;
        SDL_Surface *loadedSurface = SDL_LoadBMP(path.c_str());
        if (loadedSurface==NULL){
         cout << "Erro ou carregar imagem" << endl;
        }
        else{
            optimizedSurface = SDL_ConvertSurface(loadedSurface, screenSurface->format,0);
        }
        return optimizedSurface;
}

void close(){
    SDL_FreeSurface(keyPresSurfaceUp);
    keyPresSurfaceUp=NULL;
    SDL_FreeSurface(currentSurface);
    currentSurface=NULL;
    SDL_DestroyWindow(window);
    window=NULL;
    SDL_Quit();
}

int main (int argc, char *args []){
    if (init()){
        if (loadMedia()){
            bool quit=false;

            SDL_Event e;
            currentSurface = SDL_LoadBMP("./imagens/jojo.bmp");
            while (!quit){
                while (SDL_PollEvent(&e)!=0){
                    if( e.type == SDL_QUIT )
					{
						quit = true;
					}
					//se o usuario precionar uma tecla
                    else if( e.type == SDL_KEYDOWN )
					{
						//muda a superficie atual com base na tecla pressionada
						switch( e.key.keysym.sym )
						{
							case SDLK_UP:
							currentSurface = keyPresSurfaceUp;
							break;
                            default:
                            currentSurface =SDL_LoadBMP("./imagens/jojo.bmp");
                            break;
                       }
                    }
                }
                SDL_Rect stretchRect;
				stretchRect.x = 0;
				stretchRect.y = 0;
				stretchRect.w = SCREEN_WIDTH;
				stretchRect.h = SCREEN_HEIGHT;
				SDL_BlitScaled( currentSurface, NULL, screenSurface, &stretchRect );

                SDL_UpdateWindowSurface(window);
            }

        }
    }
    close();
    return 0;
}
