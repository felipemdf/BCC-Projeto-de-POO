#include "Game.h"

Game::Game(){}
Game::~Game(){}

void Game::init(const char *title,int xpos, int ypos, int width, int heigth, bool fullscreen){
	int flags = 0;
	if (fullscreen){
        flags= SDL_WINDOW_FULLSCREEN;
	}
	if( SDL_Init( SDL_INIT_VIDEO ) == 0 ){
		cout <<"SDL  initialize " << endl;
		if (SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY,"1")){
            cout << "Linear texture filtering was enable" << endl;
        }
        window = SDL_CreateWindow(title,xpos,ypos,width,heigth,flags);
        if (window){
            cout << "Window has been created" << endl;

        }
        renderer= SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
        if (renderer){
            cout << "Renderer has been created" << endl;
        }
        SDL_SetRenderDrawColor(renderer,0xFF,0xFF,0xFF,0xFF);
        int imgFlags=IMG_INIT_PNG;
        if( ( IMG_Init( imgFlags ) & imgFlags ) ){
            cout << "SDL_image has been initialize" << endl;

        }
        isRunning=true;
    }
    else{
        isRunning=false;
    }
}

void Game::handleEvents(){
SDL_Event events;
SDL_PollEvent(&events);
switch(events.type){
    case SDL_QUIT:
        isRunning=false;
        break;
    default:
        break;
}

}

bool Game::loadMedia(string dirImg){

        SDL_Surface* loadedSurface = IMG_Load( dirImg.c_str() );
        if( loadedSurface == NULL )
        {
            cout << "Unable to load image " << dirImg.c_str() << "SDL_image Error: " << IMG_GetError() << endl;
        }
        else{
            texture = SDL_CreateTextureFromSurface(renderer, loadedSurface );
            if( texture == NULL )
            {
                cout << "Unable to create texture from " << dirImg.c_str() << "SDL Error:: " << SDL_GetError() << endl;
            }
            SDL_FreeSurface( loadedSurface );
        }
	if( texture){
		cout << "Success to load texture image!" << endl;
	}
}
void Game::close(){
	SDL_DestroyTexture( texture );
	texture = NULL;
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow( window );
	window = NULL;
	renderer = NULL;

	IMG_Quit();
	SDL_Quit();
}

void Game::render(){
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer,texture,NULL,NULL);
    SDL_RenderPresent(renderer);

}
