#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
using namespace std;

class Game {

public:
    Game ();
    ~Game ();
    void init (const char *title,int xpos, int ypos, int width, int heigth, bool fullscreen);
    bool loadMedia(string dirImg);
    void close();
    void handleEvents();
    bool running (){return isRunning;}
    void render ();
private:
    bool isRunning;
    SDL_Window *window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Texture *texture = NULL;

};


#endif // GAME_H_INCLUDED

