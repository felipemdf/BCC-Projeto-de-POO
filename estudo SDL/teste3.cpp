#include <SDL2/SDL.h>
#include <iostream>
#include <string>
using namespace std;
//constantes de comprimento e largura da tela
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//As constantes das "superfícies" de pressionamento da tecla
enum KeyPressSurfaces
{
	KEY_PRESS_SURFACE_DEFAULT, //ENTER/PRINCIPAL
	KEY_PRESS_SURFACE_UP,
	KEY_PRESS_SURFACE_DOWN,
	KEY_PRESS_SURFACE_LEFT,
	KEY_PRESS_SURFACE_RIGHT,
	KEY_PRESS_SURFACE_TOTAL //É O TAMANHO FINAL [6]
};

//inicia o SDL e cria a janela
bool init();	

//carrega a midia
bool loadMedia();

//fecha a midia e sdl
void close();

//funçao "carregar imagem" do tipo SDL, que carrega a imagem em um path
SDL_Surface* loadSurface( std::string path );

//referencia a janela
SDL_Window* gWindow = NULL;

//referencia a superficie que vai contida na janela
SDL_Surface* gScreenSurface = NULL;

//referencia a superficie que contem a imagem correspondente a tecla, entre [] esta o tamanho do array
SDL_Surface* gKeyPressSurfaces[ KEY_PRESS_SURFACE_TOTAL ];

//referencia a imagem atual apresentada
SDL_Surface* gCurrentSurface = NULL;

bool init()
{
	//sinalizador
	bool success = true;

	//inicia o SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		cout << "SDL could not initialize/ SDL Error: "<< SDL_GetError() << endl;
		success = false;
	}
	else
	{
		//cria a janela
		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			cout << "Window could not initialize/ SDL Error: "<< SDL_GetError() << endl;
			success = false;
		}
		else
		{
			//cria a superficie da tela
			gScreenSurface = SDL_GetWindowSurface( gWindow );
		}
	}

	return success;
}

bool loadMedia()
{
	//sinalizador
	bool success = true;

	 //carrega a superficie para cada tecla
    	// chama a funçao loadSurface que se encarrega de carregar a imagem em um path e retornar outra referencia com 		   a imagem.
	gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ] = loadSurface( "./imagens/04_key_presses/press.bmp" );
	if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ] == NULL )
	{
		cout << "Failed to load default image!" << endl;
		success = false;
	}

	//superficie da tecla cima
	gKeyPressSurfaces[ KEY_PRESS_SURFACE_UP ] = loadSurface( "./imagens/04_key_presses/up.bmp" );
	if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_UP ] == NULL )
	{
		cout << "Failed to load up image!" << endl;
		success = false;
	}

	//superficie da tecla baixo
	gKeyPressSurfaces[ KEY_PRESS_SURFACE_DOWN ] = loadSurface( "./imagens/04_key_presses/down.bmp" );
	if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_DOWN ] == NULL )
	{
		cout << "Failed to load down image!" << endl;
		success = false;
	}

	//superficie da tecla esquerda
	gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT ] = loadSurface( "./imagens/04_key_presses/left.bmp" );
	if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT ] == NULL )
	{
		cout << "Failed to load left image!" << endl;
		success = false;
	}

	//superficie da tecla direita
	gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ] = loadSurface( "./imagens/04_key_presses/right.bmp" );
	if( gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ] == NULL )
	{
		cout << "Failed to load right image!" << endl;
		success = false;
	}

	return success;
}

void close()
{
	//Desaloca as superficies
	for( int i = 0; i < KEY_PRESS_SURFACE_TOTAL; ++i )
	{
		SDL_FreeSurface( gKeyPressSurfaces[ i ] );
		gKeyPressSurfaces[ i ] = NULL;
	}

	//destroi a janela
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;

	//sai do sistema SDL
	SDL_Quit();
}

SDL_Surface* loadSurface( std::string path )
{
	//carrega a imagem em um especifico path
	SDL_Surface* loadedSurface = SDL_LoadBMP( path.c_str() );
	if( loadedSurface == NULL )
	{
		cout << "Unable to load image " << path.c_str() << "SDL Error: " << SDL_GetError () << endl;
	}

	return loadedSurface;
}


int main( int argc, char* args[] )
{
	 //incia o SDL e cria a janela
	if( !init() )
	{
		cout << "Failed to initialize!" << endl;
	}
	else
	{
		//carrega a midia
		if( !loadMedia() )
		{
			cout << "Failed to load media!" << endl;
		}
		else
		{
			//loop
			bool quit = false;

			// manipulador de evento
			SDL_Event e;

			//define a superficie atual a ser mostrada, no caso o da tecla enter
			gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ];

			//enquanto o aplicativo estiver em execuçao
			while( !quit )
			{
				//enquanto tiver eventos disponiveis
				while( SDL_PollEvent( &e ) != 0 )
				{
					//se o usuario requerir a saida
					if( e.type == SDL_QUIT )
					{
						quit = true;
					}
					//se o usuario precionar uam tecla
					else if( e.type == SDL_KEYDOWN )
					{
						//muda a superficie atual com base na tecla pressionada
						switch( e.key.keysym.sym )
						{
							case SDLK_UP:
							gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_UP ];
							break;

							case SDLK_DOWN:
							gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_DOWN ];
							break;

							case SDLK_LEFT:
							gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_LEFT ];
							break;

							case SDLK_RIGHT:
							gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_RIGHT ];
							break;

							default:
							gCurrentSurface = gKeyPressSurfaces[ KEY_PRESS_SURFACE_DEFAULT ];
							break;
						}
					}
				}

				//aplica a superficie atual
				SDL_BlitSurface( gCurrentSurface, NULL, gScreenSurface, NULL );

				//atualiza a superficie
				SDL_UpdateWindowSurface( gWindow );
			}
		}
	}

	//fecha tudo
	close();

	return 0;
}
