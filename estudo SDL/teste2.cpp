#include <iostream>
#include <SDL2/SDL.h>
using namespace std;

const int TELA_LARGURA = 640;
const int TELA_COMPRIMENTO = 480;

//DECLARAÇÃO DAS FUNÇÕES
// para iniciar o SDL e criar a janela
bool inicializar ();

//para carregar uma imagem em uma tsuperficie
bool carregarMedia ();

//fechar sdl e superficies
void fechar ();

//DECLARAÇÃO DAS VARIAVEIS
//ponteiro que vai apontar para a janela
SDL_Window *window = NULL;

//ponteiro que vai apontar para a superficie da janela
SDL_Surface *screenSurface = NULL;

//ponteiro que vai apontar para uma superficie com imagem
SDL_Surface *helloWorld = NULL;

//FUNÇÕES

bool init (){
    //variavel verificadora
    bool sucesso = true;

    //inicia o sdl junto com suas especificações e verifica se deu erro
    if (SDL_Init(SDL_INIT_VIDEO)<0){
        cout << "SDL não iniciado!SDL_Erro: " << SDL_GetError() << endl;
        sucesso = false;
    }
    else{
        //cria a janela com nome,posiçao x e y, largura e comprimento e comando para mostrar tela
        window = SDL_CreateWindow("Aprendendo SDL",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,TELA_LARGURA,TELA_COMPRIMENTO,SDL_WINDOW_SHOWN);
        if (window==NULL){
          cout << "Window não iniciado!SDL_Erro: " << SDL_GetError() << endl;
          sucesso = false;
        }
        else{
            //cria a superficie da janela
            screenSurface = SDL_GetWindowSurface(window);
        }
        return sucesso;
    }
}

bool carregarMedia(){
    //variavel verificadora
    bool sucesso = true;

    //carrega no ponteiro a imagem com o nome como argumento
    helloWorld =SDL_LoadBMP("./imagens/Hello-World.bmp");
    if (helloWorld==NULL){
        cout << "Imagem nao carregada!SDL_Erro: " << SDL_GetError() << endl;
        sucesso = false;
    }
    return sucesso;
}

void fechar(){


    //retira imagem da superficie
    SDL_FreeSurface(helloWorld);
    helloWorld=NULL;

    //destroi a janela junto a superficie dela
    SDL_DestroyWindow(window);
    window=NULL;

    //sai do sdl
    SDL_Quit();
}

int main (int argc, char *args[]){
    //inicia o sdl e cria a janela
    if (!init()){
        cout << "Falha ao iniciar" << endl;
    }
    else{
        //carrega a imagem
        if(!carregarMedia()){
            cout << "falha ao carregar midia" << endl;
        }
        else {
            //aplica a imagem a superficie
            SDL_BlitSurface(helloWorld,NULL,screenSurface,NULL);

            //atualiza a superficie
            SDL_UpdateWindowSurface(window);

            //delay de 2000/1000 ms = 2s
            SDL_Delay(2000);
        }

    }
    //fecha tudo
    fechar();
    return 0;

}


