#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>
using namespace std;

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool init();
bool loadMedia();
void close();

SDL_Surface *loadMedia(string path);
SDL_Window *window=NULL;
SDL_Surface *screenSurface=NULL;
SDL_Surface *imageSurface = NULL;

bool init(){
    bool success=true;
    if(SDL_Init(SDL_INIT_VIDEO)<0){
        cout << "SDL could not initialize" << endl;
        success=false;
    }
    else{
        window = SDL_CreateWindow("teste5",SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (window==NULL){
            cout << "Window could not be create" << endl;
            success=false;
        }
        else{
            int imgFlags = IMG_INIT_PNG;
            if (!(IMG_Init(imgFlags)& imgFlags)){
                cout << "SDL_image could not initialize" << endl;
                success=false;
            }
            else{
                screenSurface= SDL_GetWindowSurface(window);
            }
        }
    }
    return success;
}


bool loadMedia(){
    bool success=true;
    imageSurface = loadMedia("imagens/jojocap.png");
    if (imageSurface==NULL){
        cout << "Failed to load PNG image" << endl;
        success=false;
    }
    return success;
}

SDL_Surface *loadMedia(string path){
    SDL_Surface *optimizedImage = NULL;
    SDL_Surface* loadedImage= IMG_Load( path.c_str() );
	if( loadedImage == NULL ){
        cout << "unable to load image" << endl;
    }
    else{
        optimizedImage = SDL_ConvertSurface(loadedImage,screenSurface->format,0);
        if (optimizedImage==NULL){
            cout << "Unable to optimize image" << endl;
        }
    SDL_FreeSurface(loadedImage);
    }
    return optimizedImage;
}

void close(){
    SDL_FreeSurface(imageSurface);
    imageSurface=NULL;
    SDL_DestroyWindow(window);
    window=NULL;

    IMG_Quit();
    SDL_Quit();
}

int main( int argc, char* args[] ){
    if (init()){
        if (loadMedia()){
            bool quit = false;
            SDL_Event e;
            while (!quit){
                while (SDL_PollEvent(&e) != 0){
                    if (e.type==SDL_QUIT){
                        quit=true;
                    }
                }
                SDL_Rect stretchRect;
                    stretchRect.x=0;
                    stretchRect.y=0;
                    stretchRect.w = SCREEN_WIDTH;
                    stretchRect.h = SCREEN_HEIGHT;
                    SDL_BlitScaled (imageSurface,NULL,screenSurface,&stretchRect);
                SDL_UpdateWindowSurface(window);
            }
        }
    }
    close();
    return 0;
}
