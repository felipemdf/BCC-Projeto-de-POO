#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

class LTexture
{
	public:
		LTexture();
		~LTexture();
		bool loadFromFile( std::string path );

		#if defined(SDL_TTF_MAJOR_VERSION)
		//Cria imagem de fonte string
		bool loadFromRenderedText( string textureText, SDL_Color textColor );
		#endif

		void free();

		//modulação da cor
		void setColor( Uint8 red, Uint8 green, Uint8 blue );

		//defini mistura
		void setBlendMode( SDL_BlendMode blending );

		//Definir modulação alfa
		void setAlpha( Uint8 alpha );

		//Renderiza a textura em um ponto
		void render( int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE );

		int getWidth();
		int getHeight();

	private:
		SDL_Texture* mTexture;
		int mWidth;
		int mHeight;
};

class LTimer
{
    public:
		//construtor
		LTimer();

		//açoes do relogio
		void start();
		void stop();
		void pause();
		void unpause();

		//Obtém o tempo do cronômetro
		Uint32 getTicks();

		//checa os status do temporizador
		bool isStarted();
		bool isPaused();

    private:
		//A hora do relógio quando o cronômetro começou
		Uint32 mStartTicks;

		//O tempo quando o cronômetro foi pausado
		Uint32 mPausedTicks;

		//status do cronometro
		bool mPaused;
		bool mStarted;
};


bool init();
bool loadMedia();
void close();
SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
//fonte usada
TTF_Font* gFont = NULL;

//Texturas de cena
LTexture gTimeTextTexture;
LTexture gPausePromptTexture;
LTexture gStartPromptTexture;

LTexture::LTexture()
{
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

LTexture::~LTexture()
{
	free();
}

bool LTexture::loadFromFile( std::string path )
{
	free();
	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if(loadedSurface)
	{
		//coloração da superficie
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture )
		{
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}

		SDL_FreeSurface( loadedSurface );
	}

	mTexture = newTexture;
	return mTexture != NULL;
}

#if defined(SDL_TTF_MAJOR_VERSION)
//carrega o texto para renderizar
bool LTexture::loadFromRenderedText(string textureText, SDL_Color textColor )
{
	free();

	//renderiza a superficie de texto (ponteiro da fonte,a textura baixada, e a cor)
	SDL_Surface* textSurface = TTF_RenderText_Solid( gFont, textureText.c_str(), textColor );
	if( textSurface != NULL )
	{
        mTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface );
		if( mTexture)
		{
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}
		SDL_FreeSurface( textSurface );
	}

	//Return success
	return mTexture != NULL;
}
#endif
