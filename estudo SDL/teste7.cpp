#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>
using namespace std;
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool init();
bool loadMedia();
void close();

//janela para renderizar
SDL_Window *window=NULL;
//renderizador da janela
SDL_Renderer* renderer = NULL;
//textura atual a ser exibida
SDL_Texture *texture = NULL;
//carregar imagem como textura
SDL_Texture *loadTexture (string path);

bool init(){
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else{
        //Sethint chama uma dica que retorna true ou false, o hint_hender_scale_quality define a qualidade do dimensionamento (0, 1=linear,2=3d)
        if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY,"1")){
            cout << "Warning: Linear texture filtering not enable" << endl;
            success=false;
        }
        window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( window == NULL ){
		cout <<"Window could not be created! SDL Error: "<< SDL_GetError() << endl;
		success = false;
	}

        else{
            //cria o renderizador para a janela usando o acelerador de hardware
            renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
            if (renderer==NULL){
                cout << "Renderer could not be created!ERRO: " << SDL_GetError() << endl;
                success=false;
            }
            else{
               //atribui cor  para atribuiçoes de desenho, como rect,line e clear;
               SDL_SetRenderDrawColor(renderer,0xFF,0xFF,0xFF,0xFF);
               //inicializa o PNG
               int imgFlags=IMG_INIT_PNG;
               if( !( IMG_Init( imgFlags ) & imgFlags ) ){
                    cout << "SDL_image could not initialize!" << endl;
                    success=false;
               }
            }
        }
	}
	return success;
}

bool loadMedia(){
    bool success=true;
    return success;
}

SDL_Texture *loadTexture(string path){
    //textura final
    SDL_Texture* newTexture=NULL;
    //carrega a imagem
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());
    if (loadedSurface==NULL){
        cout <<"Unable to load image" << endl;
    }
    else{
        //cria uma superficie com textura renderizada a partir de uma superficie existente.
        newTexture=SDL_CreateTextureFromSurface(renderer,loadedSurface);
        if (newTexture==NULL){
            cout << "unable to create texture" << endl;
        }
        SDL_FreeSurface(loadedSurface);
    }
    return newTexture;
}

void close(){
    SDL_DestroyTexture(texture);
    texture=NULL;
    SDL_DestroyRenderer(renderer);
    renderer=NULL;
    SDL_DestroyWindow(window);
    window=NULL;

    IMG_Quit();
    SDL_Quit();
}

int main( int argc, char* args[] ){
    if (init()){
        if (loadMedia()){
            bool quit = false;
            SDL_Event e;
            while (!quit){
                while (SDL_PollEvent(&e) != 0){
                    if (e.type==SDL_QUIT){
                        quit=true;
                    }
                }
                //coloração original branca
                SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0xFF, 0xFF );
                //limpa a tela com a cor designada anteriormente
                SDL_RenderClear(renderer);

                //RENDERIZA UM QUADRADO PREENCIDO DE VERMELHO(pos_x,pos_y,largura,altura)
                SDL_Rect fillRect ={SCREEN_WIDTH/4, SCREEN_HEIGHT/4, SCREEN_WIDTH/2, SCREEN_HEIGHT/2};
                //define a cor do retangulo
                SDL_SetRenderDrawColor(renderer, 0xB2, 0x22, 0x22, 0xFF);
                //ðesenha o retangulo preenchido
                SDL_RenderFillRect(renderer, &fillRect);

                //RENDERIZA UM QUADRADO VERDE NÃO PREENCHIDO
                SDL_Rect outlineRect = { SCREEN_WIDTH / 6, SCREEN_HEIGHT / 6, SCREEN_WIDTH * 2 / 3, SCREEN_HEIGHT * 2 / 3 };
                SDL_SetRenderDrawColor( renderer, 0x80, 0xFF, 0x00, 0xFF );
                SDL_RenderDrawRect( renderer, &outlineRect );

                //RENDERIZA UMA LINHA PONTILHADA AZUL DE CIMA PARA BAIXO
				SDL_SetRenderDrawColor( renderer, 0x19, 0x19, 0x70, 0xFF );
				for( int i = 0; i < SCREEN_HEIGHT; i += 4 )
				{
					SDL_RenderDrawPoint( renderer, SCREEN_WIDTH / 2, i );
				}
				//carrega a tela
				SDL_RenderPresent( renderer );
            }
        }
    }
    close();
    return 0;
}
