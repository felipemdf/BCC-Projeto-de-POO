#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//classe para embrulhar e armazenar as informações sobre a textura.
class LTexture
{
	public:
		//metodo construtor
		LTexture();

		//metodo destrutor
		~LTexture();

		//carregador de arquivos
		bool loadFromFile( std::string path );

		//desalocador de texturas
		void free();

		//renderizador que assume uma posição, (posx,poxy, retangulo que defina qual parte da imagem renderizar [null=tudo]
		void render( int x, int y, SDL_Rect *clip=NULL);

		//obtem as dimensoes das texturas
		int getWidth();
		int getHeight();

	private:
		//textura real do hardware
		SDL_Texture* mTexture;

		//dimensao das texturas
		int mWidth;
		int mHeight;
};


bool init();
bool loadMedia();
void close();


SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
//objetos da classe LTexture (são as 2 imagens)
LTexture gPressTexture;
LTexture gUpTexture;
LTexture gDownTexture;
LTexture gLeftTexture;
LTexture gRightTexture;
LTexture background;


LTexture::LTexture()
{
	//inicializa o construtor
   	//textura null
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

LTexture::~LTexture()
{
	//desaloca a textura
	free();
}

bool LTexture::loadFromFile( std::string path)
{
	//livra-se da textura existente caso haja uma
	free();

	//cria a textura final
	SDL_Texture* newTexture = NULL;

	//carrega a imagem na superficie
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
	 //cria a textura a partir dos pixels da superficie
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}
		else
		{
            //armazena as dimensoes da imagem
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;

		}

		 //desaloca a superficie com a imagem
		SDL_FreeSurface( loadedSurface );
	}

	//retorna se a textura foi carregada com sucesso
	mTexture = newTexture;
	return mTexture != NULL;
}

void LTexture::free()
{
	//libera a textura se ela existir
	if( mTexture != NULL )
	{
		//apaga e zera tudo
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

void LTexture::render( int x, int y, SDL_Rect* clip )
{
	//define o espaço de renderização e renderiza na tela
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };
	if( clip != NULL )
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}
	//essa função possui argumentos adicionais para rotação e lançamento.
	SDL_RenderCopy( gRenderer, mTexture, clip, &renderQuad );
}

int LTexture::getWidth()
{
	//retorna o valor da textura largura para quando precisar denovo
	return mWidth;
}

int LTexture::getHeight()
{
	//retorna o valor da textura largura para quando precisar denovo
	return mHeight;
}

bool init()
{
	bool success = true;

	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{

			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	bool success = true;

	if( !gPressTexture.loadFromFile( "imagens/press.png" ) )
	{
		printf( "Failed to load press texture!\n" );
		success = false;
	}

	//Load up texture
	if( !gUpTexture.loadFromFile( "imagens/up.png" ) )
	{
		printf( "Failed to load up texture!\n" );
		success = false;
	}

	//Load down texture
	if( !gDownTexture.loadFromFile( "imagens/down.png" ) )
	{
		printf( "Failed to load down texture!\n" );
		success = false;
	}

	//Load left texture
	if( !gLeftTexture.loadFromFile( "imagens/left.png" ) )
	{
		printf( "Failed to load left texture!\n" );
		success = false;
	}

	//Load right texture
	if( !gRightTexture.loadFromFile( "imagens/right.png" ) )
	{
		printf( "Failed to load right texture!\n" );
		success = false;
	}
	if( !background.loadFromFile( "imagens/fundo.png" ) )
	{
		printf( "Failed to load right texture!\n" );
		success = false;
	}

	return success;
}

void close()
{
	//libera as imagens carregadas
	gPressTexture.free();
	gUpTexture.free();
	gDownTexture.free();
	gLeftTexture.free();
	gRightTexture.free();
	background.free();


	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	IMG_Quit();
	SDL_Quit();
}

int main( int argc, char* args[] )
{
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{

		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{
			bool quit = false;
			SDL_Event e;
            //textura que ira renderizar na tela
            LTexture *currentTexture =NULL;
			while( !quit )
			{
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
					}
				}
                //Definir a textura com base no estado-chave atual
                //se a tecla estiver pressionada configuramos a textura atual para a textura correspondente. Se nenhuma das teclas
                //estiverem pressionadas, configuramos uma textura padrão.

                const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL );
				if( currentKeyStates[ SDL_SCANCODE_UP ] )
				{
					currentTexture = &gUpTexture;
				}
				else if( currentKeyStates[ SDL_SCANCODE_DOWN ] )
				{
					currentTexture = &gDownTexture;
				}
				else if( currentKeyStates[ SDL_SCANCODE_LEFT ] )
				{
					currentTexture = &gLeftTexture;
				}
				else if( currentKeyStates[ SDL_SCANCODE_RIGHT ] )
				{
					currentTexture = &gRightTexture;
				}
				else
				{
					currentTexture = &gPressTexture;
				}

				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );

				//renderiza o fundo nas seguintes posiçoes
				currentTexture->render( 0, 0 );

				//carrega a tela
				SDL_RenderPresent( gRenderer );
			}
		}
	}

	close();

	return 0;
}
