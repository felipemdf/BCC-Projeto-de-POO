#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
using namespace std;
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool init ();
bool loadMedia ();
void close ();
SDL_Texture *loadTexture (string image);

SDL_Window* window = NULL;
SDL_Renderer *renderer= NULL;
SDL_Texture *texture=NULL;

bool init(){
	bool success = true;
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
		cout << "SDL could not initialize! SDL Error:" << SDL_GetError() << endl;
		success = false;
	}
    else{
        if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY,"1")){
            cout << "Warning: Linear texture filtering not enable" << endl;
            success=false;
        }
        else{
            window=SDL_CreateWindow("teste8",SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
            if (window==NULL){
                cout << "Window could not be created!!ERROR: " << SDL_GetError() << endl;
                success=false;
            }
            else{
                renderer=SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
                if (renderer==NULL){
                    cout << "Renderer could not be created!!ERROR: " << SDL_GetError() << endl;
                    success=false;
                }
                int imgFlags=IMG_INIT_PNG;
                if(!(IMG_Init(imgFlags)& imgFlags)){
                    cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << endl;
                    success=false;
                }
            }
        }
    }
    return success;
}

bool loadMedia(){
    bool success= true;
    texture = loadTexture( "./imagens/jojocap.png" );
	if( texture == NULL )
	{
		cout << "Failed to load texture image!" << endl;
		success = false;
	}
	return success;
}

void close(){

	SDL_DestroyTexture( texture );
	texture = NULL;

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow( window );
	window = NULL;
	renderer = NULL;

	IMG_Quit();
	SDL_Quit();
}

SDL_Texture* loadTexture( string image){
	SDL_Texture* newTexture = NULL;
	SDL_Surface* loadedSurface = IMG_Load( image.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", image.c_str(), IMG_GetError() );
	}
	else
	{
        newTexture = SDL_CreateTextureFromSurface( renderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", image.c_str(), SDL_GetError() );
		}
		SDL_FreeSurface( loadedSurface );
	}

	return newTexture;
}

int main( int argc, char* args[] ){
    if( !init() ){
		printf( "Failed to initialize!\n" );
	}
	else{
		if( !loadMedia() ){
			printf( "Failed to load media!\n" );
		}
		else{
			bool quit = false;
			SDL_Event e;
		        int xImagem=0;

			while( !quit ){
				while( SDL_PollEvent( &e ) != 0 ){
					if( e.type == SDL_QUIT ){
						quit = true;
					}
					if( e.type == SDL_KEYDOWN ){
						switch( e.key.keysym.sym ){
							case SDLK_RIGHT:
							xImagem += 20;
							break;
							case SDLK_LEFT:
							xImagem -= 20;
							break;
							default:
							break;
						}
					}
				//limpa a tela com a cor definida
                SDL_SetRenderDrawColor(renderer,0xFF, 0xFF, 0xFF, 0xFF);
                SDL_RenderClear(renderer);

                //area do topo esquerdo
                //cria um objeto retangulo
                SDL_Rect topLeftViewport;
                //posicao x
                topLeftViewport.x = xImagem;
                //posicao y
                topLeftViewport.y = 0;
                //dimensao do comprimento
                topLeftViewport.w = SCREEN_WIDTH / 2;
                //dimensao da altura
                topLeftViewport.h = SCREEN_HEIGHT / 2;
		SDL_RenderSetViewport( renderer, &topLeftViewport );
		SDL_RenderCopy( renderer, texture, NULL, NULL );

		//area topo direito
                SDL_Rect topRightViewport;
                topRightViewport.x = SCREEN_WIDTH / 2;
                topRightViewport.y = 0;
                topRightViewport.w = SCREEN_WIDTH / 2;
                topRightViewport.h = SCREEN_HEIGHT / 2;
                SDL_RenderSetViewport( renderer, &topRightViewport );

                SDL_RenderCopy( renderer, texture, NULL, NULL );

				//area de baixo
		SDL_Rect bottomViewport;
				bottomViewport.x = 0;
		bottomViewport.y = SCREEN_HEIGHT / 2;
		bottomViewport.w = SCREEN_WIDTH;
		bottomViewport.h = SCREEN_HEIGHT / 2;
		SDL_RenderSetViewport( renderer, &bottomViewport );
                SDL_RenderCopy( renderer, texture, NULL, NULL );

				SDL_RenderPresent( renderer );
            }
        }


    return 0;
    }
    }
}
