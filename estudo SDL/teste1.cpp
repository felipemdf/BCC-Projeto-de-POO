#include <iostream>
#include <SDL2/SDL.h>
using namespace std;
//declara as constantes
const int TELA_LARGURA = 640;
const int TELA_COMPRIMENTO = 480;

//DECLARAÇÃO DAS FUNÇÕES
// para iniciar o SDL e criar a janela
bool inicializar ();

//para carregar uma imagem em uma tsuperficie
bool carregarMedia ();

//fechar sdl e superficies
void fechar ();


//DECLARAÇÃO DAS VARIAVEIS
//ponteiro que vai apontar para a janela
SDL_Window *window = NULL;

//ponteiro que vai apontar para a superficie da janela
SDL_Surface *screenSurface = NULL;

//sinalizador do loop
bool sair = false;

//manipulador de eventos
SDL_Event evento;

//FUNÇÕES

bool init (){
    //variavel verificadora
    bool sucesso = true;

    //inicia o sdl junto com suas especificações e verifica se deu erro
    if (SDL_Init(SDL_INIT_VIDEO)<0){
        cout << "SDL não iniciado!SDL_Erro: " << SDL_GetError() << endl;
        sucesso = false;
    }
    else{
        //cria a janela com nome,posiçao x e y, largura e comprimento e comando para mostrar tela
        window = SDL_CreateWindow("Aprendendo SDL",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,TELA_LARGURA,TELA_COMPRIMENTO,SDL_WINDOW_SHOWN);
        if (window==NULL){
          cout << "Window não iniciado!SDL_Erro: " << SDL_GetError() << endl;
          sucesso = false;
        }
        else{
            //cria a superficie da janela
            screenSurface = SDL_GetWindowSurface(window);
        }
        return sucesso;
    }
}

void fechar(){
    //destroi a janela junto a superficie dela
    SDL_DestroyWindow(window);
    window=NULL;

    //sai do sdl
    SDL_Quit();
}

int main (int argc, char *args[]){
    //inicia o sdl e cria a janela
    if (!init()){
        cout << "Falha ao iniciar" << endl;
    }
    else{
        //loop para o programa rodar ate ser pressionado alguma coisa
        while (!sair){
            //pega o evento mais recente da fila de eventos e colocar os dados do evento no SDL_Event que passamos para a função.
            while (SDL_PollEvent(&evento)!=0){
                //verifica se o tipo do evento foi o SDL_QUIT (botao de sair
                // se for, o sinalizar vira true
                if (evento.type == SDL_QUIT){
                    sair= true;
                }
            }
            //atualiza a superficie
            SDL_UpdateWindowSurface(window);
        }
    }
    //fecha tudo
    fechar();
    return 0;

}

