#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "classes/Game.h"
using namespace std;

int main (int argc, const char *argv[]){
    string dir;
    Game *game=new Game();
    game->init("usando classes",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,800,600,false);
    game->loadMedia("imagens/jojocap.png");
    while (game->running()){
        game->handleEvents();
        game->render();
    }
    game->close();
    return 0;
}

