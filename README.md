# Space Shooter
Projeto de jogo Space Shooter realizado com a finalidade avaliativa para a matéria de programação orientada a objetos do segundo semestre do curso de bacharelado em ciência da computação.

**LINGUAGEM**

    C++ 
    Biblioteca SDL2

**REQUISITOS**

    Jogador
    - 2 naves opcionais;

    Bots
    - 5 naves inimigas diferentes;
    - 2 chefões;

    Especificações dos bots
    - Cada bot deve ter um comportamento diferente em jogo;
    - Os chefões devem possuir mais atributos;

    Em jogo 
    - 2 fases;
    - 3 estágios por fase;
    - Uso de asteróide como obstaculos;

    Propriedades
    - Uso de parallax;
    - Detecção de colisão de bloco;
    - Alguns objetos do jogo irão seguir trajetórias definidas por funções;
